
@extends('layouts.app')
@section('title','Edit candidate')

@section('content')

    <body>               
        <h1>Edit candidate</h1>
        <form method = "post" action = "{{action('CandidatesController@update',$candidate->id)}}">
        @csrf
        @method('PATCH')      
          <div >
            <label for = "name">Candidate name</label>
            <input type = "text" name = "name" value = {{$candidate->name}}>
        </div>     
        <div>
            <label for = "email">Candidate email</label>
            <input type = "text" name = "email" value = {{$candidate->email}}>
        </div> 
        <div >
            <input class="btn btn-outline-dark" type = "submit" name = "submit" value = "Update candidate">
        </div>  
        @if ($errors->any())
                <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                 @endforeach
                </ul>
                </div>
            @endif                     
        </form>    
        @endsection
    </body>
</html>



